<%@page contentType="text/html;charset=utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
	<head>
	<meta charset="utf-8" />
	<link rel="stylesheet" type="text/css" href="css/index.css">
</head>
<div class="dvhead">
	<div class="dvlogo"><a href="index.html">你问我答</a></div>
	<div class="dvsearch">10秒钟注册账号，找到你的同学</div>
	<c:if test="${sessionScope.user == null}">
		<div class="dvreg">
			已有账号，立即&nbsp;<a href="login.html">登录</a>
		</div>
	</c:if>
	<c:if test="${sessionScope.user != null}">
		 <div class="dvreg">
		 	欢迎${sessionScope.user.userName}来到卓景京问答天地
		 </div>
	 </c:if>
</div>
<div class="dvContent">
	<div class="dvquesleft">

		<div class="dvqstitle">
			<image class="imgbean" src="images/bean.jpg">
			<span class="qsTitle">问答</span>
			<span class="back"><ab  href="">《《返回上一页</a></span>
		</div>
		<div class="dvtabhead">
			<div class="tabheads tabcurrent">全部问题</div>
			<div class="tabheads">我的问题</div>
			<div class="tabheads">关注问题</div>
			<div class="tabheads">问题标签</div>
		</div>
		<div class="tabContent">
			<div class="dvtags">
				<a class="curenttag">待解决</a><span class="line"></span><a>高分</a><span
					class="line"></span><a>新回答</a><span class="line"></span><a>已解决</a>
			</div>
			<div class="tab">

				<c:if test="${requestScope.questionList != null}">
				<c:forEach items="${requestScope.questionList}" var="q" varStatus="status">
				<div class="dvques">
					<div class="quesCount">
						<div class="count">${pageScope.q.answerNum}</div>
						<div class="ques">回答数</div>
					</div>
					<div class="quesContent">
						<div class="quesTitle">
							${q.reward}
							<image src="images/bean.jpg" class="bean"> <span
								class="spanques">${q.title} --- ${status.count}</span>
						</div>
						<div class="qContent">${q.content}</div>
						<div class="tags">
							<span class="tag">excel</span><span class="tag">程序</span>
						</div>
						<div class="quesUser">
							<image src="images/0.gif" class="imguser" />
							<div class="userName">
								${q.userName}
								<div class="liulan">浏览(${q.requestNum}) <!--  30分钟前-->${q.pubTime}</div>
							</div>
						</div>
					</div>
				</div>
				</c:forEach>
				</c:if>
			</div>
			<div class="tab hidden">2</div>
			<div class="tab hidden">3</div>
			<div class="tab hidden">4</div>
		</div>
	</div>
	<div class="dvquesright">
		<div>
			<buton class="btnques" onclick="location.href='add.html'">提个问题</buton>
		</div>
		<div class="dvorder">
			<div class="orderTitle">专家排行榜</div>
			<div class="users">
				<image class="userface" src="images/0.gif" />
				<div class="dvuser">
					<div class="userTitle">陈有龙</div>
					<div class="userdeital">大牛6级 豆:14006</div>
				</div>
			</div>
			<div class="users">
				<image class="userface" src="images/1.gif" />
				<div class="dvuser">
					<div class="userTitle">陈有龙</div>
					<div class="userdeital">大牛6级 豆:14006</div>
				</div>
			</div>
			<div class="users">
				<image class="userface" src="images/2.gif" />
				<div class="dvuser">
					<div class="userTitle">陈有龙</div>
					<div class="userdeital">大牛6级 豆:14006</div>
				</div>
			</div>
			<div class="users">
				<image class="userface" src="images/3.gif" />
				<div class="dvuser">
					<div class="userTitle">陈有龙</div>
					<div class="userdeital">大牛6级 豆:14006</div>
				</div>
			</div>
			<div class="users">
				<image class="userface" src="images/4.gif" />
				<div class="dvuser">
					<div class="userTitle">陈有龙</div>
					<div class="userdeital">大牛6级 豆:14006</div>
				</div>
			</div>
			<div class="users">
				<image class="userface" src="images/5.gif" />
				<div class="dvuser">
					<div class="userTitle">陈有龙</div>
					<div class="userdeital">大牛6级 豆:14006</div>
				</div>
			</div>
			<div class="users">
				<image class="userface" src="images/6.gif" />
				<div class="dvuser">
					<div class="userTitle">陈有龙</div>
					<div class="userdeital">大牛6级 豆:14006</div>
				</div>
			</div>


		</div>

	</div>

</div>
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="text/javascript">
	$(function()
	{

		$(".tabheads").click(function()
		{
			$(".tabheads").removeClass("tabcurrent").eq($(this).index()).addClass("tabcurrent");
			$(".tab").hide().eq($(this).index()).show();
		});
	});
</script>
<body>
</body>
</html>

