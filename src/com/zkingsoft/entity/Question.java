package com.zkingsoft.entity;

import java.sql.Date;

public class Question implements Entity {
	
	//问题编号
	private Integer questionId = null; 
	
	//标题
	private String title = null;
	
	//内容
	private String content = null;
	
	//作者
	private String userName = null;
	
	//发布时间
	private Date pubTime = null;
	
	//回答次数
	private Integer answerNum = null;
	
	//被请求次数
	private Integer requestNum = null;
	
	private Integer reward = null;

	public Integer getReward() {
		return reward;
	}

	public void setReward(Integer money) {
		this.reward = money;
	}

	public Integer getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getPubTime() {
		return pubTime;
	}

	public void setPubTime(Date pubTime) {
		this.pubTime = pubTime;
	}

	public Integer getAnswerNum() {
		return answerNum;
	}

	public void setAnswerNum(Integer answerNum) {
		this.answerNum = answerNum;
	}

	public Integer getRequestNum() {
		return requestNum;
	}

	public void setRequestNum(Integer requestNum) {
		this.requestNum = requestNum;
	}
	
}
