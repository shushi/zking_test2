package com.zkingsoft.entity;

import java.sql.Date;

public class User implements Entity {

	//用户名
	private String userName = null;
	
	//密码
	private String userPwd = null;
	
	//注册时间
	private Date regDate = null;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPwd() {
		return userPwd;
	}

	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}
	
}
