package com.zkingsoft.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class DBUtil {
	
	private static Properties pro = new  Properties();
	static {
//		//基于ClassPath中的相对路径来查找文件
		//DBUtil.class.getClassLoader().getResourceAsStream(name)
//		InputStream input = DBUtil.class.getResourceAsStream("/jdbc.properties");
//		
//		try {
//			pro.load(input);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		//利用国际化资源加载API读取*.proeprties ClassPath环境中
//		ResourceBundle rb = ResourceBundle.getBundle("jdbc");
//		java.util.Enumeration<String> enums =  rb.getKeys();
//		while(enums.hasMoreElements()) {
//			String key = enums.nextElement();
//			String value = rb.getString(key);
//			pro.setProperty(key, value);
//		}
		
	}
	
	
	public static void initProeprties(String path) {
		try {
			pro.load(new FileInputStream(path));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public static void getConnection( ) {
		System.out.println("driver = " + pro.getProperty("driver"));
		System.out.println("user = " + pro.getProperty("user"));
		System.out.println("password = " + pro.getProperty("password"));
	}
	
}
