package com.zkingsoft.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zkingsoft.command.SuperCommand;

public class UserCommandImpl extends SuperCommand {

	public void addUser(HttpServletRequest request,HttpServletResponse response) {
		System.out.println("添加用户信息");
	}
	
	public void updateUser(HttpServletRequest request,HttpServletResponse response) {
		System.out.println("修改用户信息");
	}
	
	public void delUser(HttpServletRequest request,HttpServletResponse response) {
		System.out.println("删除用户信息");
	}
}
