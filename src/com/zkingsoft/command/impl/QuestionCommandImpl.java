package com.zkingsoft.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zkingsoft.command.AbstractCommand;
import com.zkingsoft.entity.Question;
import com.zkingsoft.util.RequestBeanUtil;

public class QuestionCommandImpl extends AbstractCommand {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Question question = new Question();
	
		RequestBeanUtil.setProperties(request, question);
		
	}

}
