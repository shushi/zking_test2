package com.zkingsoft.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zkingsoft.command.AbstractCommand;
import com.zkingsoft.dao.IDao;
import com.zkingsoft.dao.QuestionDaoImpl;

public class LiulanCommandImpl extends AbstractCommand {

	private IDao questionDao = new QuestionDaoImpl();
	
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		List result = questionDao.findByAll();
		request.setAttribute("questionList", result);
		getRD(request,"/index.jsp").forward(request, response);
	}

}
