package com.zkingsoft.command.impl;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zkingsoft.command.AbstractCommand;

public class AjaxCommandImpl extends AbstractCommand {

	@Override
	public void execute(HttpServletRequest request, 
							HttpServletResponse response) throws Exception {
		System.out.println("userName == " + request.getParameter("userName"));
		System.out.println("userPwd == " + request.getParameter("userPwd"));
		PrintWriter writer = response.getWriter();
		writer.println("Hello World!");
		writer.close();
	}

}
