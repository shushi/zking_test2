package com.zkingsoft.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONArray;
import com.zkingsoft.command.AbstractCommand;
import com.zkingsoft.dao.IDao;
import com.zkingsoft.dao.QuestionDaoImpl;

public class AjaxLiulanCommandImpl extends AbstractCommand {

	private IDao questionDao = new QuestionDaoImpl();
	
	@Override
	public void execute(HttpServletRequest request, 
						HttpServletResponse response) throws Exception {
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		List result = questionDao.findByAll();
		//JSONObject.to
		String json = JSONArray.toJSONString(result);
		response.getWriter().println(json);
		response.getWriter().close();
	}

}
