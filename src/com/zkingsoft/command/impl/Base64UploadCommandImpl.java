package com.zkingsoft.command.impl;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Base64;
import java.util.Base64.Decoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zkingsoft.command.AbstractCommand;
public class Base64UploadCommandImpl extends AbstractCommand {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String userName = request.getParameter("userName");
		String fileName = request.getParameter("fileName");
		String fileContent = request.getParameter("fileContent");
		String realPath = request.getServletContext().getRealPath("/WEB-INF/fileupload/");
		System.out.println("userName = " + userName);
		System.out.println("fileName = " + fileName);
		System.out.println("fileContent = " + fileContent);
		
		String[] fileContents = fileContent.split(",");
		// data:image/png;base64,iVBORw0K
		// fileContents[0]           fileContents[1]
		//Base64 beas64 = Base64.;
		//将Base64进行转码
		Decoder decoder = Base64.getDecoder();
		byte[] fileBytes = decoder.decode(fileContents[1]);
		realPath = realPath + "/" + fileName;
		OutputStream output = null;
		try {
			output = new BufferedOutputStream(new FileOutputStream(realPath));
			output.write(fileBytes);
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if(output != null) {
				output.close();
			}
		}
	}

}
