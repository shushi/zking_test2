package com.zkingsoft.command.impl;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.zkingsoft.command.AbstractCommand;
import com.zkingsoft.dao.IUserDao;
import com.zkingsoft.dao.UserDaoImpl;
import com.zkingsoft.entity.User;

public class DengluCommandImpl extends AbstractCommand {
	
	private IUserDao<User> userDao = new UserDaoImpl();
	
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String userName = request.getParameter("userName");
		String userPwd = request.getParameter("userPwd");
		String validateCode = request.getParameter("validateCode");
		HttpSession session = request.getSession(true);
		String imgvc = (String)session.getAttribute("ValidateCode");
		
		if(validateCode == null || !validateCode.equalsIgnoreCase(imgvc)) {
			request.setAttribute("loginError", "您的验证码错误！");
			getRD(request,"/login.jsp").forward(request, response);
			return;
		}
		
		User user = userDao.findByNameAndPwd(userName, userPwd);
		if(user == null) {
			//没有找到用户
			request.setAttribute("loginError", "您的用户名或密码错误！");
			//response.sendRedirect("login.html");
			getRD(request,"/login.jsp").forward(request, response);
			return;
		} else {
			
			session.setAttribute("user", user);
			//跳转到成功页面
			getRD(request,"/liulan.asp").forward(request, response);
			return;
		}
	}

}
