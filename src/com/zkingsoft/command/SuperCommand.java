package com.zkingsoft.command;

import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class SuperCommand extends AbstractCommand {

	//通过http://uri?method=方法名updateUser
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String methodName = request.getParameter("method");
		if(methodName == null) {
			//正确方式应该抛异常
			throw new Exception("method参数不能空！");
		}
		
		Method method = this.getClass().getDeclaredMethod(methodName, HttpServletRequest.class,HttpServletResponse.class);
		
		method.invoke(this, request,response);
	}

}
