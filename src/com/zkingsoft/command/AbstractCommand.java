package com.zkingsoft.command;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

/**
 * 今天天气真好！Hello
 * @author shushine
 *
 */
public abstract class AbstractCommand implements Command {

	protected RequestDispatcher getRD(HttpServletRequest request,String uri) {
		return request.getServletContext().getRequestDispatcher(uri);
	}
	
	
}
