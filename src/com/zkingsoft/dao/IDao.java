package com.zkingsoft.dao;

import java.util.List;

import com.zkingsoft.entity.Entity;

public interface IDao<T extends Entity> {

	void add(T t);
	
	void update(T t);
	
	void delete(Integer pk);
	
	T findByPrimaryKey(Integer pk);
	
	List<T> findByAll();
	
}
