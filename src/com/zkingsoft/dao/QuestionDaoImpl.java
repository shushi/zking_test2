package com.zkingsoft.dao;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import com.zkingsoft.entity.Entity;
import com.zkingsoft.entity.Question;

public class QuestionDaoImpl implements IDao {

	@Override
	public void add(Entity t) {
		
	}

	@Override
	public void update(Entity t) {
		
	}

	@Override
	public void delete(Integer pk) {
		
	}

	@Override
	public Entity findByPrimaryKey(Integer pk) {
		return null;
	}

	@Override
	public List findByAll() {
		List result = new ArrayList();
		Question q = new Question();
		q.setUserName("刘勇");
		q.setTitle("涉外最帅的是不是我啊！");
		q.setContent("我是刘勇，最近有一个问题很困扰我。我究竟是不是涉外最帅的？我不想带着遗憾离开涉外,希望各位学员帮我找到答案！");
		q.setQuestionId(1);
		q.setRequestNum(999);
		q.setAnswerNum(200);
		q.setReward(2000);
		q.setPubTime(new Date(System.currentTimeMillis()));
		result.add(q);
		
		q = new Question();
		q.setUserName("李华古");
		q.setTitle("涉外最美的是不是我啊！");
		q.setContent("我是利华古，最近有一个问题很困扰我。我究竟是不是涉外最美的？我不想带着遗憾离开涉外,希望各位学员帮我找到答案！");
		q.setQuestionId(1);
		q.setRequestNum(200);
		q.setAnswerNum(100);
		q.setReward(1000);
		q.setPubTime(new Date(System.currentTimeMillis()));
		result.add(q);
		
		q = new Question();
		q.setUserName("文星");
		q.setTitle("涉外最白的是不是我啊！");
		q.setContent("我是文星，最近有一个问题很困扰我。我究竟是不是涉外最白的？我不想带着遗憾离开涉外,希望各位学员帮我找到答案！");
		q.setQuestionId(1);
		q.setRequestNum(199);
		q.setReward(200);
		q.setAnswerNum(50);
		q.setPubTime(new Date(System.currentTimeMillis()));
		result.add(q);
		return result;
	}

}
