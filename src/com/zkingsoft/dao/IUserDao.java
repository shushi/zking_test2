package com.zkingsoft.dao;

import com.zkingsoft.entity.Entity;

public interface IUserDao<T extends Entity> extends IDao<T> {

	public T findByNameAndPwd(String userName,String pwd);
	
}
